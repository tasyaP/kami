var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    watch = require('gulp-watch'),
    reload = browserSync.reload,
    autoprefixer = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    del = require('del'),
    cache = require('gulp-cache');
notify = require('gulp-notify');
pug = require('gulp-pug');
svgSprite = require('gulp-svg-sprite');

gulp.task('pug', function (done) {
    gulp.src(['app/pug/*.pug', '!app/pug/block'])
        .pipe(pug({pretty: true}).on('error', notify.onError(
            {
                message: "<%= error.message %>",
                title: "Pug Error!"
            })))
        .pipe(gulp.dest('app'))
        .pipe(browserSync.reload({stream: true}))
    done();
});

gulp.task('scss', function (done) {
    gulp.src('app/scss/main.scss')
        .pipe(sass().on('error', notify.onError(
            {
                message: "<%= error.message %>",
                title: "Sass Error!"
            })))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.reload({stream: true}))
    done();
});

gulp.task('browser-sync', function (done) {
    browserSync({
        server: {
            baseDir: './app'
        },
        browser: 'chrome'
    });
    done();
});

gulp.task('css-libs', ['scss'], function (done) {
    gulp.src('app/css/main.css')
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/css'));
    done();
});

config = {
    mode: {
        stack: {
            sprite: "./../sprite.svg"
        }
    }
};

gulp.task('svg', function () {
    gulp.src('app/i/iconSvg/*.svg')
        .pipe(svgSprite(config))
        .pipe(gulp.dest('app/i/'));
});

/*Ожидание изменений файлов для браузера*/
gulp.task('watch', ['browser-sync', 'css-libs', 'pug', 'svg'], function () {
    gulp.watch('app/scss/**/*.scss', ['scss']);
    gulp.watch('app/pug/**/*.pug', ['pug']);
    gulp.watch('app/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', browserSync.reload);
    gulp.watch('app/i/sprite.svg', browserSync.reload);
});

/*очистка папки dist перед сборкой*/
gulp.task('clean', function () {
    del.sync('js');
    del.sync('fonts');
    del.sync('icon');
    del.sync('images');
    return del.sync('css');
});

/*Дефолтный таск*/
gulp.task('default', ['watch']);

/*Очистка кэша Gulp*/
gulp.task('clear', function () {
    return cache.clearAll();
});

gulp.task('icon', function () {
    return gulp.src('app/icon/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('icon'));
});

gulp.task('images', function () {
    return gulp.src('app/images/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('images'));
});

/*Сборка проекта*/
gulp.task('build', ['clean', 'icon', 'images', 'scss', 'pug'], function(){

    var buildCss = gulp.src([
        'app/css/main.min.css'
    ])
        .pipe(gulp.dest('dist/css'));

    var buildFonts = gulp.src('app/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'));

    var buildJs = gulp.src('app/js/**/*')
        .pipe(gulp.dest('dist/js'));

    var buildHtml = gulp.src('app/index.html')
        .pipe(gulp.dest('dist'));

    gulp.task('icon', function(){
        return gulp.src('app/icon/*')
            .pipe(cache(imagemin({
                interlaced: true,
                progressive: true,
                svgoPlugins: [{removeViewBox: false}],
                use: [pngquant()]
            })))
            .pipe(gulp.dest('dist/icon'));
    });

    gulp.task('images', function(){
        return gulp.src('app/images/*')
            .pipe(cache(imagemin({
                interlaced: true,
                progressive: true,
                svgoPlugins: [{removeViewBox: false}],
                use: [pngquant()]
            })))
            .pipe(gulp.dest('dist/images'));
    });
});