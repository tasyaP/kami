$(document).ready(function () {
    $('select').select2();

    $('.tabs__item').on('click', function () {
        $('.tabs__item').removeClass('tabs__item_active');
        $(this).addClass('tabs__item_active');
        let tabs_data = $(this).data('rooms');
        $('.rooms img').each(function (index, item) {
            if ($(item).data('rooms') === tabs_data) {
                $(item).fadeIn(300);
            } else {
                $(item).hide();
            }
        });
    });

    $('.tabs-view2__item').on('click', function(){
		$('.tabs-view2__item').removeClass('tabs-view2__item_active');
		$(this).addClass('tabs-view2__item_active');
		let data = $(this).data('quest');
		$('.quest__list-wrap').each(function(index, item){
		    if($(item).data('quest') === data){
                $('.quest__list-wrap').hide();
		        $(item).show();
            }
        });
    });

    $('.js-slider').slick({
        arrows: false,
        infinite: false
    });
    $('.arrows__next').click(function () {
        $('.js-slider').slick('slickNext');
    });
    $('.arrows__prev').click(function () {
        $('.js-slider').slick('slickPrev');
    });

    $('.js-burger, .js-close').on('click', function(){
        $('.menu').toggleClass('open');
    });
});